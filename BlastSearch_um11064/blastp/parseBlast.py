#! /usr/bin/python

from Bio.Blast import NCBIXML

#result_handle = open("BlastP-restricted-J4G2S1YC015-Alignment.xml")
result_handle = open("JBH1V2KY01N-Alignment.xml")
blast_records = NCBIXML.parse(result_handle)

print("Name,Description,AlignmentLength,PercentIdentity,HSPexpect")
for blast_record in blast_records:
  for alignment in blast_record.alignments:
    for hsp in alignment.hsps:
      if hsp.expect < 0.0001:
        t = alignment.title.split(">")[0].split(" ")
        name = t[0]
        name = name.split("|")[1]
        t.pop(0)
        desc = " ".join(t)
        print "%s,'%s',%d,%f,%s" %(name, desc, hsp.align_length, float(hsp.identities) / float(hsp.align_length), hsp.expect)

